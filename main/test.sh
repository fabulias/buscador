#!/bin/bash
echo 'Iniciando pruebas'
echo -e "\n"
#variable route comienza desde aquí mismo.
#variable word es la palabra a buscar.
#out_X archivo de salida.

curl -o results/out1 -X POST -H "Content-Type: application/json" -d "{ \"word\": \"Todo\", \"route\": \"./data\"}" http://localhost:8080/search 

curl -o results/out2 -X POST -H "Content-Type: application/json" -d "{ \"word\": \"es\", \"route\": \"./data\"}" http://localhost:8080/search

curl -o results/out3 -X POST -H "Content-Type: application/json" -d "{ \"word\": \"mas\", \"route\": \"./data\"}" http://localhost:8080/search

curl -o results/out4 -X POST -H "Content-Type: application/json" -d "{ \"word\": \"bonito\", \"route\": \"./data\"}" http://localhost:8080/search

curl -o results/out5 -X POST -H "Content-Type: application/json" -d "{ \"word\": \"cuando\", \"route\": \"./data\"}" http://localhost:8080/search

curl -o results/out6 -X POST -H "Content-Type: application/json" -d "{ \"word\": \"ries\", \"route\": \"./data\"}" http://localhost:8080/search

echo -e '\n \nPruebas finalizadas'
