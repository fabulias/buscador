package main

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/fabulias/buscador/searcher"
)

type search struct {
	Word  string `json:"word"`
	Route string `json:"route"`
}

type response struct {
	Word    string
	Route   string
	Message string
	Data    searcher.PairList
}

func searchHandler(c *gin.Context) {
	var json search
	if err := c.BindJSON(&json); err == nil {
		var resp response
		resp.Route = json.Route
		resp.Word = json.Word
		result := searcher.Searcher(json.Word, json.Route)
		if len(result) == 0 {
			resp.Message = "¡Palabra no existe!"
			c.JSON(200, resp)
		} else { // Exists Word
			resp.Message = ""
			resp.Data = result
			c.JSON(200, resp)
		}
	} else {
		c.AbortWithError(http.StatusBadRequest, err)
	}
}

func main() {
	r := gin.Default()
	r.POST("/search", searchHandler)
	if err := r.Run(":8080"); err != nil {
		log.Printf("error listening on port 8080: %v", err)
	}
}
